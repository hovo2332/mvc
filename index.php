<?php

use controller\Photos;
use controller\Users;

require_once realpath("vendor/autoload.php");

  $link .= $_SERVER['HTTP_HOST'];
    
  $link = $_SERVER['REQUEST_URI'];
        
  
  $arr = explode("/",$link);

  $controllerName = $arr[1];
  $methodName = $arr[2];
  $params = [];

  
  for ($x = 3; $x < count($arr); $x++) {
   
    $params[] = $arr[$x];
  }
  $path = "controller/" . $controllerName . ".php";


  if (file_exists($path)) {
      $class = "controller\\" . $controllerName;
      $obj = new $class();
        if(method_exists($obj,$methodName)) {
            $obj->$methodName($params);
        } else if($methodName == '' && method_exists($obj,'index')) {
          $obj->index();
        } else {
          echo "Method is not defined";
        }
  }else {
    echo "Class is not defined";
  }

?>