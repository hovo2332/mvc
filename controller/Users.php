<?php

namespace controller;

class Users extends Base
{
  public function sayHi($args)
  {
    if (count($args) == 0) {
      echo "This is sayHi method of Users class with no parameters";
    } else {
      echo "This is sayHi method of Users class with this parameters: ";
      foreach ($args as $arg) {
        print_r($arg . " ");
      }
    }
  }
  public function index()
  {
    echo "This is index method of Users class";
  }
}
